#ifndef NL_CXX_UTIL_HPP_
#define NL_CXX_UTIL_HPP_

namespace nl
{
    class Noncopyable
    {
    public:
        Noncopyable() = default;
        Noncopyable(const Noncopyable &) = delete;
        Noncopyable &operator=(const Noncopyable &) = delete;
    };
}

#define NONCOPYABLE private ::nl::Noncopyable
#define COPYABLE 

#endif//NL_CXX_UTIL_HPP_
