#ifndef NL_LOG_H_
#define NL_LOG_H_

#include "../cxx/cxx_util.hpp"
#include <cstdint>
#include <cstddef>

namespace nl
{

namespace util
{
    class Logger : NONCOPYABLE
    {
    public:
        enum : std::uint8_t {
            LEVEL_VERBOSE = 0x11,
            LEVEL_DEBUG = 0x12,
            LEVEL_INFO = 0x13,
            LEVEL_WARNING = 0x14,
            LEVEL_ERROR = 0x15,
            LEVEL_FATAL = 0x16
        };
    
    public:
        Logger(std::uint32_t mask, const char* file_path, std::size_t line) noexcept;
        ~Logger();
    private:
        void put(bool val);
        void put(int8_t val);
        void put(uint8_t val);
        void put(int16_t val);
        void put(uint16_t val);
        void put(int32_t val);
        void put(uint32_t val);
        void put(const char *val);
        void put(const void *val);

        template<typename T>
        void put(const T &val)
        {
            //TODO:impl
        }
    };
}//namespace util

}//namespace nl

#endif//NL_LOGGING_H